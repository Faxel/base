#!/bin/bash
#Auteur Mr Hacker_K 


#couleur(bold)
red='\e[1;31m'
green='\e[1;32m'
yellow='\e[1;33m'
blue='\e[1;34m'
magenta='\e[1;35m'
cyan='\e[1;36m'
white='\e[1;37m'

#paquet necessaire
dependencies=( "pv" "curl" "bc" )
for i in "${dependencies[@]}"
do
    command -v $i >/dev/null 2>&1 || {
        echo -e >&2 "${yellow}$i ${white}:L'outil n'est pas encore installé "
        #essayez de l installer
        apt-get install $i -y
        echo -e "${yellow}ok tous déjà installés, attendez une minute, les outils s'exécuteront automatiquement\n${white}-Mr Hacker_K "
	sleep 2
        clear
    }
done

#données requises
host='http://sms.payuterus.biz'
cookie=$(curl -s -I "$host/alpha/send.php" | grep -o "PHPSESSID=.*" | cut -d ";" -f1)
user_agent='Mozilla/5.0 (Linux; Android 9; SM-A205F Build/PPR1.180610.011; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/75.0.3770.143 Mobile Safari/537.36'

#banniere
echo '''
			
 .----------------.  .----------------.  .----------------. 
| .--------------. || .--------------. || .--------------. |
| |    _______   | || | ____    ____ | || |    _______   | |
| |   /  ___  |  | || ||_   \  /   _|| || |   /  ___  |  | |
| |  |  (__ \_|  | || |  |   \/   |  | || |  |  (__ \_|  | |
| |   '.___`-.   | || |  | |\  /| |  | || |   '.___`-.   | |
| |  |`\____) |  | || | _| |_\/_| |_ | || |  |`\____) |  | |
| |  |_______.'  | || ||_____||_____|| || |  |_______.'  | |
| |              | || |              | || |              | |
| '--------------' || '--------------' || '--------------' |
 '----------------'  '----------------'  '----------------' 

			{ Envoyer un message }
'''
#demande
read -p $'numero : \e[1;33m' num
read -p $'\e[1;37mmessage : \e[1;33m' message

#prendre la valeur requise et prendre un captcha
data=$(curl -s "$host/alpha/index.php?a=keluar" \
			-H "User-Agent: ${user_agent}" \
			-H "Cookie: ${cookie};" \
			-H "Referer: $host/alpha/send.php")
			#pret a envoyer
			send=$(curl -s -X POST "$host/alpha/send.php" \
						-H "User-Agent: ${user_agent}" \
						-H "Cookie: ${cookie};" \
						-H "Referer: $host/alpha" \
						-d "nohp=${num}&pesan=${message}&captcha="$(echo "$data" | grep "<span>.*"  | cut -d ">" -f2 | cut -d "=" -f1 | bc)"&key="$(echo "$data" | grep -o 'name="key" value=".*' | cut -d '"' -f4))
						#if statment
						if [[ $send =~ "Envoyés des SMS gratuits " ]]; then
							echo -e "${blue}[${yellow}+${blue}] ${green}message envoyé ke${white}: ${yellow}$num${white}" | pv -qL 25
						else
							echo -e "${blue}[${red}!${blue}] ${red}message non envoyé${white}"
							echo -e "${green}possibilité${white}: \n > mauvais numéro\n > Le nombre auquel vous avez envoyé le message a atteint la limite\n > votre signal est mauvais n> et vous êtes mauvais." | pv -qL 35
						fi
#end